'use strict';

const app           = require('app');
const Menu          = require('menu');
const BrowserWindow = require('browser-window');
const shell         = require('shell');
const appName       = app.getName();

function sendAction(action, message) {
  BrowserWindow.getFocusedWindow().webContents.send(action, message);
}

const tpl = [
  {
    label: 'File',
    submenu: [
      {
        label: 'New Configuration',
        accelerator: 'CmdOrCtrl+N',
        click() {
          sendAction('new-config');
        }
      },
      {
        type: 'separator'
      },
      {
        label: `Quit`,
        accelerator: 'Ctrl+Q',
        click() {
          app.quit();
        }
      }
    ]
  },
  {
    label: 'Edit',
    submenu: [
      {
        label: 'Undo',
        accelerator: 'CmdOrCtrl+Z',
        role: 'undo'
      },
      {
        label: 'Redo',
        accelerator: 'Shift+CmdOrCtrl+Z',
        role: 'redo'
      },
      {
        type: 'separator'
      },
      {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        role: 'cut'
      },
      {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        role: 'copy'
      },
      {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        role: 'paste'
      },
      {
        label: 'Select All',
        accelerator: 'CmdOrCtrl+A',
        role: 'selectall'
      }
    ]
  },
  
  {
    label: 'Help',
    role: 'help',
    submenu: [
      {
        label: 'Website',
        click() {
          shell.openExternal('https://www.gsk.com');
        }
      },
      // {
      //   type: 'separator'
      // },
      // {
      //   label: `About ${appName}`,
      //   click() {
      //     alert('Built with Electron & React - 2016');
      //   }
      // },
    ]
  }
];

module.exports = Menu.buildFromTemplate(tpl);