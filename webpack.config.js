var path = require('path');
var webpack = require('webpack');



const PATHS = {
 app:   path.join(__dirname, 'app'              ),
 build: path.join(__dirname, 'build'            ),
 static: path.join(__dirname, 'static'            ),
 entry: path.join(__dirname, 'app', 'main.js' ),
};


module.exports = {
  devtool: 'eval',
  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    PATHS.entry
  ],
  resolve: {
    //allows to refer to file.jsx as file (without the extension)
    extensions: ['', '.js', '.jsx']
  },
  // added here, but used by devserver.js...
  contentBase: PATHS.static,
  output: {
    path: PATHS.static,
    filename: 'bundle.js',
    publicPath: "/static/",
  },
  node: {
          fs: 'empty'
  },
  externals: [
      {
          './cptable': 'var cptable'
      }
  ],

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
        DEVELOPMENT: true
    })
  ],
  module: {
    loaders: [
      {
        test: /\.jsx$|\.js$/,
        loaders: ['react-hot', 'babel'],
        include: PATHS.app
      },
      {
        test: /\.css$/,
        loader: "style!css",
        include: PATHS.app
      },
      {
        test: /\.scss$/,
        loader: "style!css!sass",
        include: PATHS.app
      },
      {
        test   : /\.(ttf|eot|svg|otf|woff(2)?)(\?[a-z0-9]+)?$/,
        loader : 'file-loader'
      },
      {
        test   : /\.(png|gif)$/i,
        loaders:  [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false' 
                  ],
      },
      {
        test  : /\.docx$/,
        loader: 'binary-loader'
      },
      {
        test  : /\.html$/,
        loader: 'html-loader',
      },
    ],
  },


};
