/*eslint-disable no-console */
import webpack from 'webpack';
import colors  from 'colors';

// Building script to automatise the generation of pre-configured apps.
// See README.md from this folder and from the ../configs folder

import config_demo    from '../configs/config.demo';
import config_testing from '../configs/config.testing';
import config_qa      from '../configs/config.qa';
import config_prod    from '../configs/config.prod';

console.log('Generating minified bundle...'.blue);

let config = {};
let config_name = "";

// get the correct config

let configFlag = process.argv.length > 1 ? process.argv[2] : '';

switch (configFlag) {
  case 'demo':
    config_name = "Demo";
    config = config_demo;
    break;
  case 'qa':
    config_name = "QA";
    config = config_qa;
    break;
  case 'prod':
    config_name = "Production";
    config = config_prod;
    break;
  case 'testing':
  default:
    config_name = "Testing";
    config = config_testing;
    break;
}

console.log(`Building ${config_name} configuration...`.blue);

// config generation:

// injects the DefinePlugin, configured to use the GLOBALS entry from the config file
let finalConfig = {
  ...config.webpackConfig, // get the actual webpackconfig from the
  plugins:[
    ...config.plugins,
    new webpack.DefinePlugin(config.GLOBALS)
  ]
};

let decorators = config.loader_decorators;

// replacing the place holders using the 'loader_decorators' entries
let loaders = config.webpackConfig.module.loaders.map( entry => {
  let newEntry = {...entry};

  if (entry.query && entry.query.name) {
    let name = entry.query.name;
    if (decorators && decorators[name]) {
      newEntry.query.name = decorators[name];
    }
  }

  return newEntry;
});

// adding the modules
finalConfig = {
  ...finalConfig,
  modules: {
    loaders
  }
};

console.log(finalConfig.modules.loaders);                                           // eslint-disable-line no-console

console.log("\nFYI, the configuration is :");                                       // eslint-disable-line no-console
console.log(finalConfig);                                                           // eslint-disable-line no-console
console.log("\nIn particular, these constant will be replaced during the build:");  // eslint-disable-line no-console
Object.keys(config.GLOBALS).map( (k) => {
  console.log(k, '--->', config.GLOBALS[k] );                                       // eslint-disable-line no-console
});

webpack(finalConfig).run((err, stats) => {
  if (err) {
    console.error(err);
    return 1;
  }
  const jsonStats = stats.toJson();

  if (jsonStats.hasErrors) {
    return jsonStats.errors.map(error => console.error(error));
  }

  if (jsonStats.hasWarnings) {
    console.log('Webpack warnings: '.bold.yellow);
    jsonStats.warnings.map(warning => console.warn(warning));
  }

  console.log(`App is compiled using the ${config_name} config in /dist !`.blue);
});
