import express from 'express';
import webpack from 'webpack';
import path from 'path';
import config from '../configs/config.dev';
import open from 'open';

const port = 3000;
const app = express();

let finalConfig = {
  ...config.webpackConfig,
  plugins:[
    ...config.plugins,
    new webpack.DefinePlugin(config.GLOBALS)
  ]
};

console.log("\nFYI, the configuration is :");                                       // eslint-disable-line no-console
console.log(finalConfig);                                                           // eslint-disable-line no-console
console.log("\nIn particular, these constant will be replaced during the build:");  // eslint-disable-line no-console
Object.keys(config.GLOBALS).map( (k) => {
  console.log(k, '--->', config.GLOBALS[k] );                                       // eslint-disable-line no-console
});

const compiler = webpack(finalConfig);

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: finalConfig.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));
app.use('/static', express.static(path.join(__dirname, '../assets')));

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '../src/index.html'));
});


app.listen(port, function (err) {
  if (err) {
    console.error(err);   // eslint-disable-line no-console
  } else {
    open(`http://localhost:${port}`);
  }
});
