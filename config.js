module.exports = {
    setupEntrypoint:  './src/main.js'
  , destination:      './dist/src'
  // , srcFiles:         './src/**/*.*'

  // , bindPolyfill: './bind-polyfill.js'
  // , buildPath: './build/'

  , bundleName: 'build.js'
  , bundleInstrumentedName: 'build-instrumented.js'
  , ARCH: 'archive.zip'
  , HTML: {
      source:           './src/*.html'
    , destination:      './dist/src/'
    , name:             'Html files'
    , task_name:        'htmlUpdate'
    , watch_task_name:  'watchHtml'
    }
  , CSS:  {
      source:           './src/css/*.css'
    , destination:      './dist/src/css/'
    , name:             'CSS files'
    , task_name:        'cssUpdate'
    , watch_task_name:  'watchCSS'
    }
  , SASS:  {
      source:           './src/sass/**/*.scss'
    , destination:      './dist/src/css/'
    , name:             'SASS files'
    , task_name:        'sassUpdate'
    , watch_task_name:  'watchSASS'
    }
  , fonts:{
      source:           './src/fonts/*.*'
    , destination:      './dist/src/fonts/'
    , name:             'fonts'
    , task_name:        'fontsUpdate'
    , watch_task_name:  'watchFonts'
    }
  , katexfonts:{
      source:           './src/css/fonts/*.*'
    , destination:      './dist/src/css/fonts/'
    , name:             'katexfonts'
    , task_name:        'katexfontsUpdate'
    , watch_task_name:  'watchKatexFonts'
    }
  , electronPack:{
      source:           './electron_pack/*.js*'
    , destination:      './dist/src/'
    , name:             'ElectronPack'
    , release_dir:      './release/'
    , output_name:      'app'
    , icon_win32:       './electron_pack/icon.ico'
  }
};

