import path from 'path';

// base to generate all the webpack configs.



export default {
  webpackConfig: {
    debug: false,
    devtool: 'none',
    noInfo: true,

    target: 'web',
    output: {
      path: path.join(__dirname,'../dist'),
      publicPath: '/',
      filename: 'bundle.js'
    },

    module: {
      noParse: [/jszip.js$/],
      loaders: [
        {test: /\.json$/, loaders: ['json']},
        {test: /\.js$/, include: path.join(__dirname, '../src'), loaders: ['babel']},
        {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
        {
          test: /\.(woff|woff2)$/,
          loader: 'url-loader',
          query: {
            prefix: 'font/',
            limit: 5000,
            name: '__FONT_NAME__'
          }
        },
        {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader',
          query: {
            limit:10000,
            mimetype:'application/octet-stream',
            name:'__FONT_NAME__'
          }
        },
        {test: /\.(css|scss)$/, loaders: ["style", "css", "sass"]},
        {test: /\.(jpe?g|png|gif|svg)$/i, loader: 'file'} //IMAGE LOADER
      ]
    }
  },
  plugins: []
};
