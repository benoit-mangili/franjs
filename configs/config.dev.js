import path     from 'path';
import webpack  from 'webpack';

process.env.NODE_ENV = 'development';

const GLOBALS = {
  // 'process.env.NODE_ENV': JSON.stringify('development'),
  // 'process.env.INTERSPEC_SERVER_URL':   JSON.stringify('https://interspec-protective.tessella.com/internal-api'),
  // 'process.env.INTERSPEC_APP_BASENAME': JSON.stringify('')
};

export default {
  webpackConfig:{
    debug: true,
    devtool: 'source-map',
    noInfo: false,
    target: 'web',
    output: {
      path: path.join(__dirname,'../dist'),
      publicPath: '/',
      filename: 'bundle.js'
    },

    node: {
      fs: 'empty'
    },

    entry: [
      'eventsource-polyfill',
      'webpack-hot-middleware/client?reload=true',
      path.resolve(__dirname, '../src/index')
    ],

    externals: [
      {
        "./cptable": "var cptable"
      }
    ],

    devServer: {
      contentBase: path.resolve(__dirname, '../src')
    },

    resolve: {
      alias: {
        jszip: 'xlsx/jszip.js'
      }
    },

    module: {
      noParse: [/jszip.js$/],
      loaders: [
        {test: /\.json$/, loaders: ['json']},
        {test: /\.js$/, include: path.join(__dirname, '../src'), loaders: ['babel']},
        {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
        {
          test: /\.(woff|woff2)$/,
          loader: 'url-loader',
          query: {
            prefix: 'font/',
            limit: 5000,
            name: '__FONT_NAME__'
          }
        },
        {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader',
          query: {
            limit:10000,
            mimetype:'application/octet-stream',
            name:'__FONT_NAME__'
          }
        },
        {test: /\.(css|scss)$/, loaders: ["style", "css", "sass"]},
        {test: /\.(jpe?g|png|gif|svg)$/i, loader: 'file'} //IMAGE LOADER
      ]
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  GLOBALS: {
    ...GLOBALS
  },
  loader_decorators: {
    '__FONT_NAME__' : ''
  }
};
