import React from 'react';
import Autocomplete from 'react-autocomplete'
import find from 'lodash/find';

export let styles = {
  item: {
    padding: '2px 6px',
    cursor: 'default'
  },

  highlightedItem: {
    color: 'white',
    background: 'hsl(200, 50%, 50%)',
    padding: '2px 6px',
    cursor: 'default'
  },

  menu: {
    border: 'solid 1px #ccc'
  }
}




export function matchToTerm (item, value) {
  return (
    item.value.toLowerCase().indexOf(value.toLowerCase()) !== -1
  )
}

export function sortNames (a, b, value) {
  return (
    a.value.toLowerCase().indexOf(value.toLowerCase()) >
    b.value.toLowerCase().indexOf(value.toLowerCase()) ? 1 : -1
  )
}

class PersonAutoComplete extends React.Component {
  constructor(props, context){
    super(props, context);
    this.state = {};
  }
  getValue(){
    let value = this.refs['autocomplete'].refs['input'].value;
    if (value) {
      return find(this.props.items, (item) => item.value === value).key;
    }
    return '';
  }

  render() {
    let {items} = this.props;
    return (
      <Autocomplete
          ref='autocomplete'
          initialValue=""
          items={items}
          value={this.state.value}
          getItemValue={ (item) => {
            console.log("?", item);
            return item.value} }
          shouldItemRender={matchToTerm}
          sortItems={sortNames}
          onSelect={value => this.setState({ value })}
          onChange={(event, value) => this.setState({ value })}

          renderItem={(item, isHighlighted) => (
            <div
              style={isHighlighted ? styles.highlightedItem : styles.item}
              key={item.key}
            >{item.value}</div>
          )}
        />
    );
  }
}

export default PersonAutoComplete;
