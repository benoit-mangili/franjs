import {REHYDRATE} from 'redux-persist/constants';


export default function reducer(state = {}, action) {
  switch (action.type) {
    case 'LOAD_COUPLES':
    case 'PURGE_ARRANGEMENT':
      return {};
    case 'LOAD_SCORE':
      return {score:action.payload};
    default:
      return state;
  }
}

