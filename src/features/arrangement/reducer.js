import {REHYDRATE} from 'redux-persist/constants';

let LoadArrangement = (state={}, arrangements) => {
    // let cs = arrangements.reduce( (acc, c, i) => {
    //   acc[i] = {
    //     fullname: c.fullname,
    //     so_fullname: c.so_fullname,
    //     id: i
    //   };
    //   return acc;
    // }, {});

    return {...arrangements};
}

export default function reducer(state = {}, action) {
  switch (action.type) {
    case 'LOAD_ARRANGEMENT':
      return LoadArrangement(state, action.payload);
    case 'ADD_TABLE':
    case 'ADD_COUPLE':
    case 'ADD_FEELING':
    case 'DEL_TABLE':
    case 'DEL_COUPLE':
    case 'DEL_FEELING':
    case 'LOAD_COUPLES':
    case 'PURGE_ARRANGEMENT':
      return {};
    case REHYDRATE:
      return {...state, ...action.payload.arrangements};

    default:
      return state;
  }
}

