import React, {Component, PropTypes} from 'react';

import Couple from '../couple/couple';
import _ from 'lodash';

export default class Arrangement extends Component {
  makeSeats (couples, table_size, w, h, r, mr){
    let j = 0;
    return _.map(couples, (c, i) => {

              let s = 0;
              let rx = mr;
              let ry = mr;
              if (c.so_fullname) {
                s = 0.5;
                ry = 3.0 * ry;
              }



              let a = 360 * (j + s) / table_size;

              const el =  <g transform={`translate(${w / 2},${h / 2}), rotate(${a}), translate(${r},0)`} className={c.so_fullname ? 'couple' : 'single'} >
                { c.so_fullname ?

                  <g>
                    <circle cx='0' cy='-20' r={rx}/>
                    <circle cx='0' cy='+20' r={rx}/>
                  </g>



                  : <circle cx='0' cy='0' r={rx}/>

                }
                          </g>;

              if (c.so_fullname) {
                j+=1;
              }
              j+=1;

              return el;
            });
  }

  makeFeelings(couples, feelings, table_size, w, h, r, mr){

    // console.log("feelings", feelings);


    if (feelings.length > 0) {
      let j = 0;
      let angles = _.reduce(couples, (memo, c, i) => {

        let s = 0;
        if (c.so_fullname) {
          s = 0.5;
        }

        let a = 360 * (j + s) / table_size;

        if (c.so_fullname) {
          j+=1;
        }
        j+=1;
        memo[c.id] = a * Math.PI / 180;
        return memo;
      }, {});
      // angles = {'id_46' : 0, 'id_47': 90};
      return <g>
        {
          feelings.map((f) => {
            {/*console.log(" angles ", angles[f.id1], angles[f.id2] );*/}
            const x1 = r * Math.cos(angles[f.id1]) + w/2;
            const y1 = r * Math.sin(angles[f.id1]) + h/2;

            const x2 = r * Math.cos(angles[f.id2]) + w/2;
            const y2 = r * Math.sin(angles[f.id2]) + h/2;

            {/*console.log(x1, y1, x2, y2);*/}


            const dx = (x1+x2) / 2 ; //- 23.6/2;
            const dy = (y1+y2) / 2 ; //- 23.6/2;

            const heart = "M23.6,0     c-3.4,0 -6.3,2.7 -7.6,5.6 C14.7,2.7,   11.8,0,  8.4,0     C3.8,0,   0,3.8,    0,8.4             c0,9.4,9.5,11.9,16,21.2"+
              "c6.1-9.3,16-12.1,16-21.2C32,3.8,28.2,0,23.6,0z";

            {/*const heart =     "M23.6,0     l-7.6,5.6                      L8.4,0                                          L0,8.4               l16,21.2"+*/}
            {/*"l16,-21.2    L23.6,0   z";*/}

            {/*const heart2 = "m0,0  c-3.4,0  -6.3,2.7 -7.6,5.6 c -1.3,-2.9, -4.2,-5.6, -7.6,-5.6   c-3.8,0,  -8.4,-0,  -8.4,8.4   l16,21.2 l16,-21.2   l-8.4,-8.4" ;//         c3.8,0,0,3.8,0,8.4c0,9.4,9.5,11.9,16,21.2"+*/}
            {/*//                      "c6.1-9.3,16-12.1,16-21.2"; //c32,3.8,28.2,0,23.6,0z";*/}


            const lightning = "m-10,-25, l20,-5 l-10,20 l20,-5 l-25,50 l5,-35     l-20,5";


            return  <g transform={`translate(${dx}, ${dy})`}>
              <line x1={x1-dx} y1={y1-dy} x2={x2-dx} y2={y2-dy} />
              {
                f.F === 'love'
                  ? <g className='heart' >
                    <path d={heart}/>
                  </g>
                  : <g className='lightning' >
                    <path d={lightning}/>
                  </g>
              }
            </g>;
          })
        }
      </g>;

    }

    return <g/>;
  }

  makeMissingFeelings(couples, feelings, table_size, w, h, r, mr){

    // console.log("feelings", feelings);


    if (feelings.length > 0) {
      let j = 0;
      let angles = _.reduce(couples, (memo, c, i) => {

        let s = 0;
        if (c.so_fullname) {
          s = 0.5;
        }

        let a = 360 * (j + s) / table_size;

        if (c.so_fullname) {
          j+=1;
        }
        j+=1;
        memo[c.id] = a * Math.PI / 180;
        return memo;
      }, {});

      // angles = {'id_46' : 0, 'id_47': 90};
      return <g>
        {
          feelings.map((f) => {

            let a = angles[f.id1] ? angles[f.id1] : angles[f.id2];

            const x = r * Math.cos(a) + w/2;
            const y = r * Math.sin(a) + h/2;

            const heart = "M23.6,0     c-3.4,0 -6.3,2.7 -7.6,5.6 C14.7,2.7,   11.8,0,  8.4,0     C3.8,0,   0,3.8,    0,8.4             c0,9.4,9.5,11.9,16,21.2"+
              "c6.1-9.3,16-12.1,16-21.2C32,3.8,28.2,0,23.6,0z";


            return  <g transform={`translate(${ x }, ${ y })`}>
              {
                <g className='heart' >
                  <path d={heart}/>
                </g>
              }
            </g>;
          })
        }
      </g>;

    }

    return <g/>;
  }

  render () {
    let {arrangement, setSwap, feelings} = this.props;
    let w = 400;
    let h = 350;
    let r = 90;
    let mr = 20;


    let people = _.chain(arrangement.couples).map(c=> [c.fullname, c.so_fullname]).flatten().filter( c => c ).value();

    let couple_ids = arrangement.couples.map(c=>c.id);

    const localFeelings = feelings.filter(f => _.includes(couple_ids, f.id1) && _.includes(couple_ids, f.id2) );
    const missingFeelings = feelings.filter(f => {

      if (f.F === 'hatred'){ return false; }

      const f1 = _.includes(couple_ids, f.id1);
      const f2 = _.includes(couple_ids, f.id2);

      return (f1 && !f2) || (!f1 && f2);
    });



    return  <li className="uk-list arrangementItem">
              <div className="">
                <div className="title">
                  {arrangement.tablename} ( {arrangement.size} people )
                </div>
                <svg width={w} height={h}>
                  <circle cx={w/2} cy={h/2 + 50} r={r} className='tableCloth' onClick={() => setSwap(arrangement.table_id, undefined) }/>
                  <text x={w/2} y={h/2 + 20 + 50} textAnchor="middle" className="bigNumber" fontSize={60} onClick={() => setSwap(arrangement.table_id, undefined) } >{people.length}</text>
                  <g className='peoples'>
                    {
                      this.makeSeats(arrangement.couples, arrangement.size, w, h+100, r, mr)
                    }
                  </g>
                  <g className='feelings'>
                    {
                      this.makeMissingFeelings(arrangement.couples, missingFeelings, arrangement.size, w, h+100, r, mr)
                    }
                  </g>
                  <g className='feelings'>
                    {
                      this.makeFeelings(arrangement.couples, localFeelings, arrangement.size, w, h+100, r, mr)
                    }
                  </g>
                  <g className='listOfPeople'>
                  {
                    _.map(arrangement.couples, (c, i) => {
                      let y = 18 * i;
                      let tt = c.fullname + ( c.so_fullname ? '+1' : '' );
                      let isHead = false;
                      let swap = () => {};

                      if (arrangement.head && c.fullname === arrangement.head.fullname) {
                        tt += ' HEAD';
                      } else {
                        swap = () => setSwap(arrangement.table_id,  c.id);
                      }

                      const heart = "M23.6,0     c-3.4,0 -6.3,2.7 -7.6,5.6 C14.7,2.7,   11.8,0,  8.4,0     C3.8,0,   0,3.8,    0,8.4             c0,9.4,9.5,11.9,16,21.2"+
                        "c6.1-9.3,16-12.1,16-21.2C32,3.8,28.2,0,23.6,0z";

                      let missingLove = _.find(missingFeelings, (f) => f.id1 === c.id || f.id2 === c.id ) ;

                      return  <g transform={`translate(${30},${y+20})`} className='bullet' >
                                {
                                  missingLove &&  <g transform={`translate(${0},${0})`} className='littleHeart'  ><g transform={`translate(${-16},${-14})`} > <path d={heart}/> </g> </g>
                                }
                                <circle cx='0' cy='-5' r='7' onClick={ swap } />
                                <text x={10} y={0} fontSize="14">{tt} </text>
                              </g>;
                    })
                  }
                  </g>
                  <defs>
                    <filter id="fancy-goo">
                      <feGaussianBlur in="SourceGraphic" stdDeviation="7" result="blur" />
                      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0    0 1 0 0 0    0 0 1 0 0    0 0 0 10 -7" result="goo" />
                      <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                    </filter>
                  </defs>
                </svg>
              </div>
            </li>;
  }
}






//
// <ul className="cirle-container">
//   {
//     _.map(this.props.arrangement.couples, ( (individual, key) => {
//
//       return <li key={key}><div>{individual.fullname}</div></li>;
//       // return <Couple
//       //           key={key}
//       //           id={key}
//       //           fullname={individual.fullname}
//       //           so_fullname={individual.so_fullname}
//       //         />
//     }))
//   }
// </ul>