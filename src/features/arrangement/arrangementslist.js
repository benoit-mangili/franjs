import React, {Component, PropTypes} from 'react';

import { connect } from 'react-redux';
import * as actionCreators from './actions';
import { bindActionCreators } from 'redux';
import Arrangement from './arrangement';

import * as coupleSelectors from '../couple/selectors';
import * as feelingsSelectors from '../feeling/selectors';
import * as tablesSelectors from '../table/selectors';
import * as arrangementsSelectors from './selectors';
import Feeling from '../feeling/feeling';


export class ArrangementsList extends Component {

  constructor(props, context){
    super(props, context);
    this.gen = this.gen.bind(this);
    this.purge = this.purge.bind(this);
    this.swap = this.swap.bind(this);
    this.setSwap = this.setSwap.bind(this);
    this.removeSwap = this.removeSwap.bind(this);
    this.state={};
  }

  gen(){
    this.props.actions.generateNewArrangement();
  }

  purge(){
    this.props.actions.purgeArrangement();
  }

  swap(t1, c1, t2, c2){
    this.props.actions.swap(t1, c1, t2, c2);
  }

  setSwap(t, c){
    if (this.state.t1) {
      console.log(this.state.t1, this.state.c1," <-> ", t, c);
      this.swap(this.state.t1, this.state.c1, t, c);
      this.removeSwap();
    } else {
      this.setState({t1: t, c1: c});
    }
  }

  removeSwap(){
    this.setState({t1:null, c1: null});
  }

  render(){
    let {arrangements, couples, tables, score, feelings} = this.props;
    return <div>

                { this.state.c1 && <div className="coupleSelected" onClick={this.removeSwap}> about to swap {couples[this.state.c1].fullname} with ... </div> }

              <div>
                <button className='uk-button' onClick={this.gen} >
                  <i className="uk-icon-cog" />
                </button>
                ---- {score.score} ----
                <button className='uk-button' onClick={this.purge} >
                  <i className="uk-icon-trash" />
                </button>
              </div>

              <ul className="stuff">
                {
                  feelings.map(( (f, key) => {
                    let feeling = {...f};
                    feeling['fullname1'] = couples[feeling.id1].fullname;
                    feeling['fullname2'] = couples[feeling.id2].fullname;
                    return <Feeling
                      key={key}
                      id={key}
                      feeling={feeling}
                      {...this.props.actions}
                    />
                  }))
                }
              </ul>


              <div className="arrangementWrapper">
                <ul className="arrangementHolder">
                  {
                    Object.keys(arrangements).map( key => {
                      let a = arrangements[key];
                      let arrangement = {
                        table_id:   a.table_id,
                        tablename:  tables[a.table_id].tablename,
                        size:       tables[a.table_id].size,
                        head:       couples[tables[a.table_id].head_id]
                      };

                      arrangement.couples = a.couples.map( couple_id => couples[couple_id] );
                      return <Arrangement
                                key={key}
                                id={key}
                                feelings={feelings}
                                arrangement={arrangement}
                                setSwap={this.setSwap}
                                {...this.props.actions}
                              />
                    })
                  }
                </ul>
              </div>

            </div>;
  }
}


const mapStateToProps = (state) => {
  // console.log(state)
  return {
    tables:       tablesSelectors.getAll(state),
    couples:      coupleSelectors.getAll(state),
    arrangements: arrangementsSelectors.getAll(state),
    feelings:     feelingsSelectors.getAll(state),
    score:        arrangementsSelectors.getScore(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  }
}

export const ArrangementsListContainer = connect(
  mapStateToProps
 ,mapDispatchToProps
)(ArrangementsList);


