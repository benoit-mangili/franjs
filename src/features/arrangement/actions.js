import generateArrangement from '../core';
import {swapCouples} from '../core';

export function loadArrangement(payload) {
  return {
    type: 'LOAD_ARRANGEMENT',
    payload
  };
}
export function loadScore(payload) {
  return {
    type: 'LOAD_SCORE',
    payload
  };
}

export function purgeArrangement(payload) {
  return {
    type: 'PURGE_ARRANGEMENT',
    payload
  };
}

export function generateNewArrangement() {
  return function (dispatch, getState) {
    let res = generateArrangement(getState());
    dispatch(loadArrangement(res.arrangements));
    dispatch(loadScore(res.score));
  };
}


export function swap(t1, c1, t2, c2) {
  return function (dispatch, getState) {
    let res = swapCouples(getState(), t1, c1, t2, c2);
    dispatch(loadArrangement(res.arrangements));
    dispatch(loadScore(res.score));
  };
}