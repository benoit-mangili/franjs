import React from 'react';
import { connect } from 'react-redux';
import * as actionCreators from './couple/actions';
import { bindActionCreators } from 'redux';

import {Component, PropTypes} from 'react';
import Dropzone from 'react-dropzone';
import jszip from 'jszip';
import XLSX from 'xlsx';


export default class Uploader extends Component {

  onDrop (files) {
    console.log(files[0])

    var reader = new FileReader();

    var name = files[0].name;
    let that = this;
    reader.onload = function(e) {
      var data = e.target.result;
      var wb = XLSX.read(data, {type: 'binary'});
      /* DO SOMETHING WITH workbook HERE */
      console.log(wb.SheetNames)

      var ws = wb.Sheets[ wb.SheetNames[0] ];
      let listOfPersons = XLSX.utils.sheet_to_json(ws);
      console.log(listOfPersons);
      listOfPersons = listOfPersons.map( p => {
        return {
          fullname : p['Staff Name'],
          so_fullname : p['Name of +1'],
        }
      })

      that.props.actions.loadCouples( listOfPersons ) ;
      // for (let z in worksheet) {
        /* all keys that do not begin with "!" correspond to cell addresses */
        // if(z[0] === '!') continue;
        // console.log(  "!" + z + "=" + JSON.stringify(worksheet[z].v));
      // }

    };

    reader.readAsBinaryString(files[0]);
    // this.props.actions.loadfile(  ) ; 
  }


  render () {
    return  <div className="uk-grid">
              <Dropzone onDrop={this.onDrop.bind(this)}>
                <div>
                  DROP HERE!
                </div>
              </Dropzone>
            </div>
  }
}


const mapStateToProps = (state) => {
  return {
    // tables: state.get('tables').toJS(),
    // couples: state.get('couples').toJS(),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  }
}

export const UploaderContainer = connect(
  mapStateToProps 
 ,mapDispatchToProps
)(Uploader);
