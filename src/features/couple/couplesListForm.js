import React, {PropTypes} from 'react';
import {Field, reduxForm, FieldArray} from 'redux-form';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div className="uk-width-3-10">
      <input {...input} type={type} placeholder={label}/> {touched && error && <span>{error}</span>}
    </div>
)

const renderCouples = ({ fields, meta: { touched, error } }) => (
  <ul>
    {fields.map((couple, index) => {
        return  <li key={index}>
                  <div className="uk-grid">
                    <div className="uk-width-1-10">
                      Couple #{index}
                    </div>
                    <Field name={`${couple}.fullname`}    type="text" component={renderField} label="Name"/>
                    <Field name={`${couple}.so_fullname`} type="text" component={renderField} label="SO"/>
                    <div className="uk-width-1-10">
                      <button type="button" className="uk-button" title="Remove Couple" onClick={() => fields.remove(index)}>
                        <i className="uk-icon-trash" />
                      </button>
                    </div>
                  </div>
                </li>
      }
    )}
    <li>
      <button type="button" onClick={() => fields.push({})}>Add Couple</button>
      {touched && error && <span>{error}</span>}
    </li>
  </ul>
)

const CouplesListForm = ({
  handleSubmit,
  onSave }) => (
  <form className="form-horizontal"  onSubmit={handleSubmit}>
    <FieldArray name="couples" component={renderCouples}/>

    <div className="form-group">
      <div className="btn-toolbar pull-right">
        <button type="button" className="btn btn-primary" onClick={handleSubmit(onSave)}>Save</button>
      </div>
    </div>
  </form>);

CouplesListForm.propTypes = {
  onSave: PropTypes.func,
};

export default reduxForm({form: 'CouplesListForm'})(CouplesListForm);
