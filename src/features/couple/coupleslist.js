import React, {Component, PropTypes} from 'react';

import { connect } from 'react-redux';
import * as actionCreators from './actions';
import { bindActionCreators } from 'redux';
import Couple from '../couple/couple';
import * as selectors from './selectors';

import CoupleForm from './couplesListForm';

export class CouplesList extends Component {
  constructor(props, context){
    super(props, context);
    this.saveFormContent = this.saveFormContent.bind(this);
  }

  saveFormContent(data){
    // let fullname    = this.refs.fullname.value;
    // let so_fullname = this.refs.so_fullname.value;
    this.props.actions.loadCouples(data.couples);
  }

  render(){
    let {couples, actions} = this.props;

    let initialValues = {
      couples: Object.keys(couples).sort((a,b) => a.fullname - b.fullname).map( (key) => couples[key] )
    };

    return <div>

              <CoupleForm initialValues={initialValues} onSave ={ this.saveFormContent }/>

              {/*<div>*/}
                {/*<form className="uk-form">*/}
                  {/*<input ref='fullname' placeholder="Full name"/>*/}
                  {/*<input ref='so_fullname' placeholder="+1's Full name"/>*/}
                  {/*<button className='uk-button' onClick={this.handleAddPerson}>*/}
                    {/*<i className="uk-icon-plus" />*/}
                  {/*</button>*/}
                {/*</form>*/}



              {/*</div>*/}
            </div>;
  }
}


const mapStateToProps = (state) => {
  return {
    couples: selectors.getAll(state) || {} , // state.get('couples').toJS(),
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  }
};

export const CouplesListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CouplesList);