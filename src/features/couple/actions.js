export function addCouple(payload) {
  return {
    type: 'ADD_COUPLE',
    payload
  };
}

export function delCouple(payload) {
  return {
    type: 'DEL_COUPLE',
    payload,
  };
}

export function loadCouples(payload) {
  return {
    type: 'LOAD_COUPLES',
    payload
  };
}

