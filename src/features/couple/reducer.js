
import {REHYDRATE} from 'redux-persist/constants';


let loadCouples = (state={}, couples) => {
  let cs = couples.reduce( (acc, c, i) => {
    acc["id_" + i] = {
      fullname: c.fullname,
      so_fullname: c.so_fullname,
      id: "id_" + i
    };
    return acc;
  }, {});

  return cs;
}

let addCouple = (state={}, couple) => {
  let {fullname, so_fullname} = couple;

  let id = Math.random().toString(36).substr(2, 5);
  let c = {...state};

  c[id] = {
    fullname,
    so_fullname
  };

  return c;
};

// specification reducer
export default function reducer(state = {}, action) {
  switch (action.type) {
    case 'ADD_COUPLE':
      return addCouple(state, action.payload); //state.update('couples', subState => Core.addCouple(fromJS(subState), action.fullname, action.so_fullname));
    case 'UPD_COUPLE':
      return {};//state.update('couples', subState => Core.updCouple(fromJS(subState), action.id, action.fullname, action.so_fullname));
    case 'DEL_COUPLE':
      let s = {...state};
      delete s[action.payload];
      return s;
    //state.update('couples', subState => Core.delCouple(fromJS(subState), action.id));
    case 'LOAD_COUPLES':
      console.log("state", action);
      return loadCouples(state, action.payload); //Core.loadCouples(state, action.couples);

    case REHYDRATE:
      return {...state, ...action.payload.couples};


    default:
      return state;
  }
}

