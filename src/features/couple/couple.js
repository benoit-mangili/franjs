import React, {Component, PropTypes} from 'react';

export default class Couple extends Component {
	render () {
    let {id, delCouple, fullname, so_fullname, idx} = this.props;

		return  <li className="">
              <div className="uk-grid">
                <div className="uk-width-9-10">
                  { idx && (idx + ' / ') }
                  { fullname }
                  { so_fullname && " & " + so_fullname }
                </div>
                  <div className="uk-width-1-10">
                    <button className='uk-button' onClick={()=> delCouple(id)} >
                      <i className="uk-icon-trash" />
                    </button>
                  </div>
              </div>
            </li>;
	}
}
