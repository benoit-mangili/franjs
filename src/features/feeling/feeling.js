import React, {Component, PropTypes} from 'react';

export default class Feeling extends Component {
  render () {
    let {feeling} = this.props;

    return  <li className="uk-list">
              <div className="uk-grid">
                <div className="uk-width-4-10">
                  { feeling.fullname1 }
                </div>
                <div className="uk-width-1-10">
                  { ( feeling.F === 'love' )
                      ? <i className='uk-icon-heart uk-icon-justify'/>
                      : <i className='uk-icon-bolt uk-icon-justify' />
                  }
                </div>
                <div className="uk-width-4-10">
                  { feeling.fullname2 }
                </div>
                { this.props.delFeeling &&
                      <div className="uk-width-1-10">
                        <button className='uk-button' onClick={() => this.props.delFeeling(this.props.id)}>
                          <i className="uk-icon-trash"/>
                        </button>
                      </div>
                }
              </div>
            </li>;
  }
}