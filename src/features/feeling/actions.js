
export function addFeeling(payload) {
  return {
    type: 'ADD_FEELING',
    payload
  };
}

export function delFeeling(payload) {
  return {
    type: 'DEL_FEELING',
    payload,
  };
}

