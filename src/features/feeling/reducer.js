import {REHYDRATE} from 'redux-persist/constants';


let addFeeling = (state=[], feeling) => {

  if (feeling.id1 && feeling.id2) {
    return [...state, feeling];
  }

  return state;
};

// specification reducer
export default function reducer(state = [], action) {
  switch (action.type) {
    case 'ADD_FEELING':
      return addFeeling(state, action.payload); //state.update('couples', subState => Core.addCouple(fromJS(subState), action.fullname, action.so_fullname));
    // case 'UPD_COUPLE':
    //   return {};//state.update('couples', subState => Core.updCouple(fromJS(subState), action.id, action.fullname, action.so_fullname));
    case 'DEL_FEELING':
      let s = [...state];
      s.splice(action.payload, 1);
      return s;
    //state.update('couples', subState => Core.delCouple(fromJS(subState), action.id));
    case REHYDRATE:
      return [...state, ...(action.payload.feelings||[]) ];
    case 'LOAD_COUPLES':
      return [];

    default:
      return state;
  }
}

