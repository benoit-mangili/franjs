import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
// import {Button, Glyphicon} from 'react-bootstrap';

import * as coupleSelectors from '../couple/selectors';
import * as feelingsSelectors from './selectors';

import * as actionCreators from './actions';
import { bindActionCreators } from 'redux';
import Feeling from './feeling';
import Autocomplete from '../shared/autocomplete';


export class FeelingsList extends Component {
  constructor(props, context){
    super(props, context);
    this.handleAddFeeling = this.handleAddFeeling.bind(this);
  }

  handleAddFeeling (e) {
    e.preventDefault();
    let id1 = this.refs.id1.getValue();
    let id2 = this.refs.id2.getValue();
    let F = this.refs.feeling.value;
    this.props.actions.addFeeling({id1, id2, F});
  }

  render() {
    let {couples} = this.props;
    console.log("couples", couples);
    let items = Object.keys(couples)
                      .map( k => ({
                        key: k,
                        value: couples[k].fullname
                      }));
    console.log("items", items);
    return <div>
              <div className="uk-grid">
                <div className="uk-width-4-10">

                  <ul>
                        {
                          this.props.feelings.map(( (f, key) => {
                            let feeling = {...f};
                            feeling['fullname1'] = couples[feeling.id1].fullname;
                            feeling['fullname2'] = couples[feeling.id2].fullname;
                            return <Feeling
                                      key={key}
                                      id={key}
                                      feeling={feeling}
                                      {...this.props.actions}
                                    />
                          }))
                        }
                  </ul>
                </div>
              </div>
              <div>
                <form className="uk-form">
                  <Autocomplete ref='id1' items={items} />
                  <select ref='feeling'>
                    <option value='love'>loves</option>
                    <option value='hatred'>hates</option>
                  </select>
                  <Autocomplete ref='id2' items={items} />
                  <button className='uk-button' onClick={this.handleAddFeeling}>
                    <i className="uk-icon-plus" />
                  </button>
                </form>
              </div>
            </div>;
  }
}


const mapStateToProps = (state) => {
  return {
    feelings: feelingsSelectors.getAll(state),
    couples:  coupleSelectors.getAll(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  }
}

export const FeelingsListContainer = connect(
  mapStateToProps
 ,mapDispatchToProps
)(FeelingsList);


