import React, {PropTypes} from 'react';
import {Field, reduxForm, FieldArray} from 'redux-form';
import Autocomplete from '../shared/autocomplete';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div className="uk-width-2-10">
      <input {...input} type={type} placeholder={label}/> {touched && error && <span>{error}</span>}
    </div>
)

const renderSelectInTable = ({ input, label, options, disabled, meta: { touched, invalid, error } }) => (
    <div className="uk-width-2-10">
      <select {...input} >
        {
          options.map((e, i) => {
            return <option key={i} value={e.Id}>{e.Name}</option>;
          })
        }
      </select>
      <div className="text-danger">
        {touched ? error: ''}
      </div>
  </div>);


const renderTables = ({ fields, coupleList, meta: { touched, error } }) => (
  <ul>
    <li>
      <button type="button" onClick={() => fields.push({})}>Add Table</button>
      {touched && error && <span>{error}</span>}
    </li>
    {fields.map((table, index) => {
        return  <li key={index}>
                  <div className="uk-grid">
                    <div className="uk-width-1-10">
                      Table #{index}
                    </div>
                    <Field name={`${table}.tablename`} type="text" component={renderField} label="Tablename"/>
                    <Field name={`${table}.size`}      type="text" component={renderField} label="size"/>
                    <Field name={`${table}.head_id`}   type="select" options={coupleList} component={renderSelectInTable} />

                    <div className="uk-width-1-10">
                      <button type="button" className="uk-button" title="Remove Table" onClick={() => fields.remove(index)}>
                        <i className="uk-icon-trash" />
                      </button>
                    </div>
                  </div>
                </li>
      }
    )}
  </ul>
)

const TablesListForm = ({
  coupleList,
  handleSubmit,
  onSave }) => (
  <form className="form-horizontal"  onSubmit={handleSubmit}>
    <FieldArray name="tables" component={renderTables} coupleList={coupleList}/>
    <div className="form-group">
      <div className="btn-toolbar pull-right">
        <button type="button" className="btn btn-primary" onClick={handleSubmit(onSave)}>Save</button>
      </div>
    </div>
  </form>);

TablesListForm.propTypes = {
  onSave: PropTypes.func,
};

export default reduxForm({form: 'TablesListForm'})(TablesListForm);
