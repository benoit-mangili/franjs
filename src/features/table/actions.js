export function addTable(payload) {
  return {
    type: 'ADD_TABLE',
    payload
  };
}

export function delTable(payload) {
  return {
    type: 'DEL_TABLE',
    payload,
  };
}


export function loadTables(payload) {
  return {
    type: 'LOAD_TABLES',
    payload
  };
}
