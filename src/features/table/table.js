import React, {Component, PropTypes} from 'react';

export default class Table extends Component {
  render () {
    return  <li className="">
              <div className="uk-grid">
                <div className="uk-width-9-10">
                  { this.props.table.tablename } - 
                  { this.props.table.headname } - 
                  { this.props.table.size }
                </div>
                <div className="uk-width-1-10">
                  <button className='uk-button' onClick={()=> this.props.delTable(this.props.id)} > 
                    <i className="uk-icon-trash" />
                  </button>
                </div>
              </div>
            </li>;
  }
}
