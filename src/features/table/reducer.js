import {REHYDRATE} from 'redux-persist/constants';


let addTable = (state={}, table) => {
  let {tablename, size, head_id} = table;

  let id = Math.random().toString(36).substr(2, 5);
  let c = {...state};

  c[id] = { tablename, size, head_id };
  return c;
};


let loadTables = (state={}, tables) => {
  let ts = tables.reduce( (acc, t, i) => {
    console.log("red", t );
    acc[i] = {
      tablename: t.tablename,
      size: t.size,
      id: i
    };

    if (t.head_id !== "-1") {
      acc[i].head_id = t.head_id;
    }

    return acc;
  }, {});

  return ts;
}



// specification reducer
export default function reducer(state = {}, action) {
  switch (action.type) {
    case 'ADD_TABLE':
      return addTable(state, action.payload); //state.update('couples', subState => Core.addCouple(fromJS(subState), action.fullname, action.so_fullname));
    // case 'UPD_COUPLE':
    //   return {};//state.update('couples', subState => Core.updCouple(fromJS(subState), action.id, action.fullname, action.so_fullname));
    case 'DEL_TABLE':
      let s = {...state};
      delete s[action.payload];
      return s;
    //state.update('couples', subState => Core.delCouple(fromJS(subState), action.id));
    case 'LOAD_TABLES':

      console.log("state", action);
      return loadTables(state, action.payload); //Core.loadCouples(state, action.couples);

    case REHYDRATE:
      return {...state, ...action.payload.tables};
    case 'LOAD_COUPLES':
      return {
      '0': {
        tablename: 'All About Eve',
          size: '12',
          id: '0'
      },
      '1': {
        tablename: 'The Godfather',
          size: 10,
          id: '1'
      },
      '2': {
        tablename: 'Casablanca',
          size: 10,
          id: '2'
      },
      '3': {
        tablename: 'The Silence of the Lambs',
          size: 10,
          id: '3'
      },
      '4': {
        tablename: 'Lawrence of Arabia',
          size: 10,
          id: '4'
      },
      '5': {
        tablename: 'It Happened One Night',
          size: 10,
          id: '5'
      },
      '6': {
        tablename: 'The Deer Hunter',
          size: 10,
          id: '6'
      },
      '7': {
        tablename: 'Gone With the Wind',
          size: 10,
          id: '7'
      },
      '8': {
        tablename: 'One Flew Over the Cuckoo’s Nest',
          size: 10,
          id: '8'
      },
      '9': {
        tablename: 'From Here to Eternity',
          size: 10,
          id: '9'
      },
      '10': {
        tablename: 'My Fair Lady',
          size: 10,
          id: '10'
      },
      '11': {
        tablename: 'The French Connection',
          size: 10,
          id: '11'
      },
      '12': {
        tablename: 'The Apartment ',
          size: 10,
          id: '12'
      },
      '13': {
        tablename: 'In the Heat of the Night',
          size: 10,
          id: '13'
      },
      '14': {
        tablename: 'On the Waterfront',
          size: 10,
          id: '14'
      },
      '15': {
        tablename: 'Annie Hall',
          size: 10,
          id: '15'
      },
      '16': {
        tablename: 'Argo',
          size: '10',
          id: '16'
      }
    };
    default:
      return state;
  }
}

