import React from 'react';
import { connect } from 'react-redux';
// import {Button, Glyphicon} from 'react-bootstrap';
import * as actionCreators from './actions';
import * as arrangementactions from '../arrangement/actions';
import { bindActionCreators } from 'redux';
import Table from './table';
import Autocomplete from '../shared/autocomplete';

import * as coupleSelectors from '../couple/selectors';
import * as feelingsSelectors from '../feeling/selectors';
import * as tablesSelectors from './selectors';

import TableForm from './tablesListForm';


import _ from 'lodash';

export const TablesList = React.createClass({
  handleAddTable: function(e){
    e.preventDefault();
    let tablename = this.refs.tablename.value;
    let size = this.refs.size.value;
    let head_id = this.refs.head_id.getValue();

    if (tablename === "") return;
    if (size === "") return;
    if (head_id === "") return;

    this.props.actions.addTable({tablename, size, head_id});
  },

  generateTables: function(e){
    e.preventDefault();
    let nbTable = this.refs.nbTable.value;
    let sizeTable = this.refs.sizeTable.value;
    nbTable = parseInt(nbTable);
    sizeTable = parseInt(sizeTable);

    if (isNaN(nbTable) || isNaN(sizeTable)) return;

    _.times(nbTable, (i) => {
        this.props.actions.addTable({tablename: 'Table '+i, size: sizeTable})
      }
    )

  },

  saveFormContent(data){
    // let fullname    = this.refs.fullname.value;
    // let so_fullname = this.refs.so_fullname.value;
    console.log("data", data);
    this.props.actions.loadTables(data.tables);

    this.props.arrangementactions.purgeArrangement();

  },


  render: function(){
    let {tables,couples, actions} = this.props;
    let coupleList = [
      {Id:-1, Name:'Nobody'},
      ...Object.keys(couples)
        .map( k => ({
          Id: k,
          Name: couples[k].fullname
        }))
    ];


    let couple_sizes = _.reduce(couples, (acc, c, c_id) => {
      acc[c_id] = c.so_fullname ? 2 : 1 ;
      return acc;
    }, {});

    let nb_people = _.reduce(couple_sizes, (memo, s) => memo + s, 0);


    let initialValues = {
      tables: Object.keys(tables).map( (key) => tables[key] )
    };

    console.log("init", initialValues);
    return <div>
              { nb_people } are coming.
      {/*<input ref='nbTable' type='text' placeholder='nb table' />*/}
      {/*<input ref='sizeTable' type='text' placeholder='table size' />*/}
              {/*<button className='uk-button' onClick={this.generateTables}>*/}
                {/*<i className="uk-icon-plus" />*/}
              {/*</button>*/}
              <TableForm initialValues={initialValues} coupleList={coupleList} onSave ={ this.saveFormContent }/>

              {/*<ul>*/}
                {/*{ */}
                  {/*Object.keys(tables).map( key => {*/}
                    {/*let table = {...tables[key]};*/}
                    {/*if (table.head_id && couples[table.head_id]) {*/}
                      {/*table.headname = couples[table.head_id].fullname;*/}
                    {/*}*/}
                    {/*return <Table*/}
                              {/*key={key}*/}
                              {/*id={key} */}
                              {/*table={table}*/}
                              {/*{...actions}*/}
                            {/*/>*/}
                  {/*})*/}
                {/*}*/}
              {/*</ul>*/}
              {/*<div>*/}
                {/*<form className="uk-form">*/}
                  {/*<input ref='tablename' type='text' placeholder='table name' />*/}
                  {/*<input ref='size' type='text' placeholder='size' />*/}
                  {/*<Autocomplete ref='head_id' items={ items } />*/}
                  {/*<button className='uk-button' onClick={this.handleAddTable}>*/}
                    {/*<i className="uk-icon-plus" />*/}
                  {/*</button>*/}
                {/*</form>*/}
              {/*</div>*/}
            </div>;
  }
});






const mapStateToProps = (state) => {
  return {
    tables: tablesSelectors.getAll(state),
    couples: coupleSelectors.getAll(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch),
    arrangementactions: bindActionCreators(arrangementactions, dispatch)
  }
}

export const TablesListContainer = connect(
  mapStateToProps
 ,mapDispatchToProps
)(TablesList);


