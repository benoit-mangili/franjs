import React, {PropTypes} from 'react';
import {Link} from 'react-router';

class App extends React.Component {
  render() {
    return (
      <div>
        <div className='Menu'>
          <Link to="/">Home?</Link>
          <Link to="/uploader">Uploader</Link>
          <Link to="/couples">Couples</Link>
          <Link to="/feelings">Feelings</Link>
          <Link to="/tables">Tables</Link>
          <Link to="/arrangements">Arrangements</Link>
        </div>
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired,
};

export default App;
