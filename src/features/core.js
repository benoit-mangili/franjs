
import _ from 'lodash';

export const swapCouples = (state, t1, c1, t2, c2) => {
  const {arrangements, couples, tables, feelings} = state;

  const couple_sizes = getCoupleSizes(couples);
  const s1 = get_seat(arrangements, couple_sizes, t1, c1);
  const s2 = get_seat(arrangements, couple_sizes, t2, c2);

  const new_arrangements = swap(s1, s2, arrangements, tables, couple_sizes);

  return {
    arrangements : new_arrangements,
    score: score(new_arrangements, feelings, getLoveNotMet(feelings) )
  };
}

const get_seat = (arrangements, couple_sizes, table_id, couple_id) => {
  const arr_id = _.findKey(arrangements, arrangement => arrangement.table_id === table_id);
  const arr = arrangements[arr_id];
  const couple_size = (couple_id && couple_sizes[couple_id]) || 0;
  return {arr_id, couple_id, table_id, nb_seat_of_couple: couple_size, seat_left: arr.seat_left};
};


export const swap = (s1, s2, arrangements, tables, couple_sizes) => {

  const s1_nb_seat_if_leaving = s1.seat_left + s1.nb_seat_of_couple;
  const s2_nb_seat_if_leaving = s2.seat_left + s2.nb_seat_of_couple;

  if (s1.arr_id === s2.arr_id) {
    return arrangements;
  }

  const table_id1 = arrangements[s1.arr_id].table_id;
  const table_id2 = arrangements[s2.arr_id].table_id;

  if (s1.couple_id && tables[table_id1].head_id && tables[table_id1].head_id === s1.couple_id) {
    return arrangements;
  }

  if (s2.couple_id && tables[table_id2].head_id && tables[table_id2].head_id === s2.couple_id) {
    return arrangements;
  }


  if (!s1.couple_id && !s2.couple_id) {
    return arrangements;
  }

  if ( (s1_nb_seat_if_leaving >= s2.nb_seat_of_couple)  && (s2_nb_seat_if_leaving >= s1.nb_seat_of_couple) ) {

    let new_r = _.cloneDeep(arrangements);

    const replace_couple = (arr, new_couple_id, previous_couple_id, from) => {
      arr.couples = arr.couples.filter( v => v !== previous_couple_id );
      arr.seats = [];

      if (new_couple_id) { arr.couples.push(new_couple_id); }
      arr = add_size_to_one_arrangement(arr, tables, couple_sizes);
      if (arr.seat_left < 0){
        console.log("after addition", arr, 'removing ', previous_couple_id, 'adding', new_couple_id, 'from', from);
        console.log("new_r[s1.arr_id]", new_r[s1.arr_id], "new_r[s2.arr_id]", new_r[s2.arr_id]);
        console.log("!!!", arr );
      }
      return arr;
    };

    new_r[s1.arr_id] = replace_couple(new_r[s1.arr_id], s2.couple_id, s1.couple_id     ,s2.arr_id);
    new_r[s2.arr_id] = replace_couple(new_r[s2.arr_id], s1.couple_id, s2.couple_id     ,s1.arr_id);

    return new_r;
  }

  return arrangements;
};

export const add_size_to_one_arrangement = (arr, tables, couple_sizes) => {

  let capacity  = tables[arr.table_id].size;

  let occupency = _.reduce(arr.couples, (memo2, c_id) => {
    return memo2 + couple_sizes[c_id];
  }, 0 );

  let seats = _.reduce(arr.couples, (memo2, c_id) => {
    memo2.push(c_id);
    if(couple_sizes[c_id] ==2) {
      memo2.push(c_id);
    }
    return memo2;
  }, [] );

  let seat_left = capacity - occupency;

  return {...arr, capacity, seat_left, seats};
};

export const add_size_to_arrangements = (arrangements, tables, couple_sizes) => {
  return _.reduce(arrangements, (memo, arr, a_id) => {
    memo[a_id] = add_size_to_one_arrangement(arr, tables, couple_sizes);
    return memo;
  }, {});

};

export const getCoupleSizes = (couples) => {
  return _.reduce(couples, (acc, c, c_id) => {
    acc[c_id] = c.so_fullname ? 2 : 1 ;
    return acc;
  }, {});
}

const scoreFeeling = (table_arrangement, feelings) => {
  return _.reduce(feelings, (acc, feeling) => {
    if (feeling.F === 'love') {
      if (_.includes(table_arrangement.couples, feeling.id1)
        && _.includes(table_arrangement.couples, feeling.id2)) {
        // console.log( " -> L ", feeling );
        acc -= 10;
      }
    } else if (feeling.F === 'hatred') {
      if (_.includes(table_arrangement.couples, feeling.id1)
        && _.includes(table_arrangement.couples, feeling.id2)) {
        // console.log( " -> H ", feeling );
        acc += 10000;
      }
    }
    return acc;
  }, 0);
}

const score = (arrangements, feelings, loveNotMet) => {
  return _.reduce(arrangements, (s, table_arrangement) => {
    s += scoreFeeling(table_arrangement,feelings);
    return s;
  }, loveNotMet * 10 );
};


const getLoveNotMet = (feelings) =>  _.reduce(feelings, (memo, feeling) => {
  return memo + (feeling.F === 'love' ? 1 : 0);
}, 0);



export default function(state){

  let {feelings, tables, couples, arrangements} = state

  let couple_sizes = getCoupleSizes(couples);

  const total_nb_person = _.reduce(couple_sizes, (m,s) => m+s, 0);

  let table_ids = _.keys(tables);
  let nb_tables = table_ids.length;

  const table_size_available = _.map(table_ids, (id) => {
    if (tables[id].head_id) {
      return tables[id].size - couple_sizes[tables[id].head_id];
    }
    return tables[id].size;
  });

  const table_with_heads = _.map(table_ids, (id)=>{
    if (tables[id].head_id) {
      return { table_id: id, couples: [ tables[id].head_id ] }
    }
    return { table_id: id, couples: [] }
  });

  const heads = _.filter(_.map(tables, t=> t.head_id), h=>h);

  const couple_for_shuffle = _.filter(couples, c => !_.includes( heads, (c.id).toString() ));

  const get_random_arrangements = () => {

    let couples_ids     = _.chain({...couple_for_shuffle}).map(c=> (c.id).toString() ).shuffle().value();
    let new_arrangement = _.cloneDeep(table_with_heads);
    let table_size_left = [...table_size_available];
    let not_full_table_ids = [...table_ids];

    // console.log(table_size_left)
    while (couples_ids.length > 0){
      // console.log("couples left... ", couples_ids.length);
      let id = couples_ids.shift();
      // console.log("dealing with ", id );
      let couple_size = couple_sizes[ id ];

      let couple_has_been_placed = false;
      let nb_of_tries = 0;
      // console.log("id", id);
      while(!couple_has_been_placed && nb_of_tries < 100) {

        let random_table_id = _.shuffle(not_full_table_ids).shift();
        // console.log("table_id", random_table_id);
        let space_left = table_size_left[random_table_id];
        if (space_left >= couple_size) {
          // console.log("placed", id, ' on table ', random_table_id, couples[id]);
          // console.log(" seat before ", table_size_left[random_table_id]);
          new_arrangement[random_table_id].couples.push(id);
          table_size_left[random_table_id] -= couple_size;
          // console.log(" seat left ", table_size_left[random_table_id]);
          couple_has_been_placed = true;
        }

        if (table_size_left[random_table_id] === 0){
          not_full_table_ids = not_full_table_ids.filter( t => t !== random_table_id );
          // console.log("removing table", random_table_id , not_full_table_ids);
        }


        nb_of_tries += 1;
      }





      if (!couple_has_been_placed) {
        console.error('SOMETHING WENT HORRIBLY WRONG!', new_arrangement, id, couple_size, couples );
      }
    }

    return {...new_arrangement};
  }

  const loveNotMet = getLoveNotMet(feelings);


  const get_random_seat = (arrangements) => {
    const arr_id = _.chain(arrangements).keys().shuffle().first().value();
    const arr = arrangements[arr_id];
    const seat = _.random(0,  table_size_available[arr.table_id] - 1);

    const couple_id = arr.seats[seat];
    const couple_size = (couple_id && couple_sizes[couple_id]) || 0;

    return {arr_id, couple_id, table_id: arr.table_id, nb_seat_of_couple: couple_size, seat_left: arr.seat_left};
  };

  const random_swap = (arrangements) => {
    const s1 = get_random_seat(arrangements);
    const s2 = get_random_seat(arrangements);
    return swap(s1, s2, arrangements, tables, couple_sizes);
  };


  let current_score = 9999999;
  let current_arrangement = null;
  let a = arrangements;

  if (Object.keys(a).length === 0) {
    current_arrangement = {...get_random_arrangements()};
    a = current_arrangement;

    console.log("get random", a);
  } else {
    current_arrangement = a ;
    current_score = score(a, feelings, loveNotMet);
  }

  for( let j = 1; j >= 10 ; j--){
    a = {...get_random_arrangements()};
    // console.log(a);
    let s = score(a, feelings, loveNotMet);
    if (s < current_score) {
      console.log("score", s);
      current_score = s;
      current_arrangement = a;
    }
    if (s <= 100) break;
  }

  a = add_size_to_arrangements(a, tables, couple_sizes);

  for( let j = 1000; j >= 0 ; j--){
    a = _.cloneDeep(random_swap( _.cloneDeep(a) ));
    // console.log(a);
    let s = score(a, feelings, loveNotMet);
    // console.log("score", s);
    if (s <= current_score) {
      console.log("score", s);
      current_score = s;
      current_arrangement = a;
    }
    if (s <= 0) break;
  }

  return {arrangements:{...current_arrangement}, score: current_score};
}
