import React                        from 'react'                                  ;
import {Route, IndexRoute}          from 'react-router'                           ;
import App                          from './features/App'                         ;
import {TablesListContainer}        from './features/table/tableslist'            ;
import {CouplesListContainer}       from './features/couple/coupleslist'          ;
import {FeelingsListContainer}      from './features/feeling/feelingslist'        ;
import {ArrangementsListContainer}  from './features/arrangement/arrangementslist';
import {UploaderContainer}          from './features/uploader'                    ;



export default (
  <Route path="/" components={App}>
    <Route name='uploader'     path="/uploader"     component={UploaderContainer} />
    <Route name='tables'       path="/tables"       component={TablesListContainer} />
    <Route name='couples'      path="/couples"      component={CouplesListContainer} />
    <Route name='arrangements' path="/arrangements" component={ArrangementsListContainer} />
    <Route name='feelings'     path="/feelings"     component={FeelingsListContainer} />

  </Route>
);

