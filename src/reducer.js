import {combineReducers}      from 'redux';

import coupleReducer from './features/couple/reducer';
import feelingReducer from './features/feeling/reducer';
import tableReducer from './features/table/reducer';
import arrangementReducer from './features/arrangement/reducer';
import scoreReducer from './features/arrangement/scoreReducer';
import {reducer as form}      from 'redux-form';


const root = combineReducers({
  couples : coupleReducer,
  feelings: feelingReducer,
  tables: tableReducer,
  arrangements: arrangementReducer,
  score: scoreReducer,
  form: form

});

export default root;
