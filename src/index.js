import React from 'react';
import ReactDOM from 'react-dom';
import {Router, browserHistory} from 'react-router';
import {createStore} from 'redux';
import reducer from './reducer';

import {Provider} from 'react-redux';
// import {TablesListContainer}  from './components/table/tableslist';
// import {CouplesListContainer}  from './components/couple/coupleslist';
// import {FeelingsListContainer} from './components/feeling/feelingslist';
// import {ArrangementsListContainer} from './components/arrangement/arrangementslist';
// import {UploaderContainer} from './components/uploader';
// import {App} from './components/app';
//
//
// import {List, Map, fromJS} from 'immutable';
// import $ from 'jquery';
// import _ from 'lodash';
//
// window.jQuery = $;
// window._ = _;
import routes from "./routes";


import configureStore from "./store/configureStore"; // eslint-disable-line import/default


import './main.scss';

//
// const routes = <Route path='/' name='app' component={App}>
//   <Route name='uploader'     path="/uploader"        component={UploaderContainer} />
//   <Route name='tables'  path="/tables"  component={TablesListContainer} />
//   <Route name='couples' path="/couples" component={CouplesListContainer} />
//   <Route name='arrangements' path="/arrangements" component={ArrangementsListContainer} />
//   <Route name='feelings' path="/feelings" component={FeelingsListContainer} />
// </Route>;

const store = configureStore();
//
// const store = createStore(reducer, Map(),
//   window.devToolsExtension ? window.devToolsExtension() : undefined
// );
//
//
// store.dispatch({
//   type: 'LOAD_COUPLES',
//   payload: [
//     {id: 'A', fullname: 'Mr  A', so_fullname: 'Mrs O' },
//     {id: 'B', fullname: 'Mrs B'                       },
//     {id: 'C', fullname: 'Mrs C', so_fullname: 'Mrs P' },
//     {id: 'D', fullname: 'Mr  D', so_fullname: 'Mr Q'  },
//   ],
// });



// store.dispatch({
//   type: 'INIT',
//   data: fromJS({
//     couples: {
//       'A': {id: 'A', fullname: 'Mr  A', so_fullname: 'Mrs O' },
//       'B': {id: 'B', fullname: 'Mrs B'                       },
//       'C': {id: 'C', fullname: 'Mrs C', so_fullname: 'Mrs P' },
//       'D': {id: 'D', fullname: 'Mr  D', so_fullname: 'Mr Q'  },
//     },
//     feelings: [
//       // { id1:'A', id2:'B', feeling: 'love'},
//       // { id1:'A', id2:'C', feeling: 'hatred'},
//     ],
//     tables: {
//       '1':  {tablename:'table  1', size: 10},
//       '2':  {tablename:'table  2', size: 10},
//       '3':  {tablename:'table  3', size: 10},
//       '4':  {tablename:'table  4', size: 10},
//       '5':  {tablename:'table  5', size: 10},
//       '6':  {tablename:'table  6', size: 10},
//       '7':  {tablename:'table  7', size: 10},
//       '8':  {tablename:'table  8', size: 10},
//       '9':  {tablename:'table  9', size: 10},
//       '10': {tablename:'table 10', size: 10},
//       '11': {tablename:'table 11', size: 10},
//       '12': {tablename:'table 12', size: 10},
//       '13': {tablename:'table 13', size: 10},
//       '14': {tablename:'table 14', size: 10},
//       '15': {tablename:'table 15', size: 10},
      {/*'16': {tablename:'table 16', size: 10},*/}
      {/*'17': {tablename:'table 17', size: 10},*/}
      {/*'18': {tablename:'table 18', size: 10},*/}
      {/*'19': {tablename:'table 19', size: 10},*/}
      {/*// 't1': {tablename:'table 1', size: 10, head_id: 'A'},*/}
      {/*// 't2': {tablename:'table 2', size: 10, head_id: 'B'},*/}
      {/*// 't3': {tablename:'table 3', size: 10, head_id: 'C'},*/}
    {/*},*/}
    {/*arrangements: [*/}
      {/*// {*/}
      {/*//   table_id: 't1',*/}
      {/*//   couples: [*/}
      {/*//     'A',*/}
      {/*//     'B',*/}
      {/*//     'C',*/}
      {/*//   ],*/}
      {/*// },*/}
      {/*// {*/}
      {/*//   table_id: 't2',*/}
      {/*//   couples: [*/}
//       //     'D',
//       //     'A',
//       //     'C',
//       //   ],
//       // }
//     ]
//   })
// });

ReactDOM.render(
    <Provider store={store}>
      <Router history={browserHistory} routes={routes}/>
    </Provider>,
    document.getElementById('ReactEntry')
);





// (function(win){
//   "use strict";

  // win.jQuery = require('jquery');
//   win.$ = win.jQuery;
//   win._ = require('lodash');


//   var React = require('react');
//   var ReactDOM = require('react-dom');
//   var Top = require('./components/top');
  
//   ReactDOM.render(
//     <Provider store={store}>
//     <Top />
//     </Provider>,

//     document.getElementById('ReactEntry')
//   );

// })(window);
