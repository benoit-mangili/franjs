import {compose, createStore, applyMiddleware} from 'redux';
import {persistStore} from 'redux-persist';
import rootReducer from '../reducer';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
// import {constants as PrefConstants} from '../features/preferences';
// import {constants as AuthConstants} from '../features/auth';

function createConfiguredStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(thunk, reduxImmutableStateInvariant()),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );
}

export default function configureStore(initialState) {
  let configuredStore = createConfiguredStore(initialState);
  persistStore(configuredStore);
  return configuredStore;
}
