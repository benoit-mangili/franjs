import chai from 'chai';
import chaiImmutable from 'chai-immutable';
chai.use(chaiImmutable);

import {expect} from 'chai';
import {List, Map} from 'immutable';
import Immutable from 'immutable';

describe('immutability', () => {

  
  describe('updateIn()', () => {
    it('...1', () => {
      let data = Immutable.fromJS({a:{b:6}});
      let data2= data.updateIn(['a', 'b'], value => value * 3);
      expect(data2.get('a').get('b')).to.equal(18);
    });
    it('...2', () => {
      let data = Immutable.fromJS({a:{}});
      let data2= data.updateIn(['a', 'b', 'c'], 0, value => value + 1);
      expect(data2.get('a').get('b').get('c')).to.equal(1);
    });
  });


  describe('a tree', () => {
    function addMovie(currentState, movie){
      return currentState.set('movies',
        currentState.get('movies').push(movie));
    }

    it('is immutable', () => {
      let state = Map({
        movies: List.of('A', 'B')
      });

      let nextState = addMovie(state, 'C');

      expect(nextState).to.equal(Map(
        {movies:List.of('A', 'B', 'C')})
      );
      expect(state).to.equal(Map(
        {movies:List.of('A', 'B')}
      ))
    });
  });


  describe('A List', () => {

    function addMovie(currentState, movie) {
      return currentState.push(movie);
    }

    it('is immutable', () => {
      let state = List.of('Trainspotting', '28 Days Later');
      let nextState = addMovie(state, 'Sunshine');

      expect(nextState).to.equal(List.of(
        'Trainspotting',
        '28 Days Later',
        'Sunshine'
      ));
      expect(state).to.equal(List.of(
        'Trainspotting',
        '28 Days Later'
      ));
    });

  });

});
