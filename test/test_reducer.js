import chai from 'chai';
import chaiImmutable from 'chai-immutable';
chai.use(chaiImmutable);

import _ from 'lodash';
import {expect} from 'chai';
import {List, Map, fromJS} from 'immutable';
import Immutable from 'immutable';

import reducer from '../src/reducer';

{
  persons: {
    'A': {id: 'A', fullname: 'A a', plusone: true },
    'B': {id: 'B', fullname: 'cc bc', plusone: true },
    'C': {id: 'C', fullname: 'C c', plusone: true },
    'D': {id: 'D', fullname: 'Cha c', plusone: true },
  },
  feelings: [
    { A:'A', B:'B', feeling: 'love'},
    { A:'B', B:'C', feeling: 'hatred'},
  ],
  tables: {
    't1': {name:'t1', size: 10, head: 'A'},
    't2': {name:'t2', size: 10, head: 'B'},
    't3': {name:'t3', size: 10, head: 'C'},
  },
  arrangement: {
    'table_1': ['A',],
    'table_2': ['B',],
    'table_3': ['C',],          
  }
}


describe('reducer', () => {
  
  it('handles ADD_FEELING', () => {
    let initialState = fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
      ]
    });
    let action = {type:'ADD_FEELING', feeling: { emotion: 'love', A: 'C', B: 'D' } };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
        { emotion: 'love', A: 'C', B: 'D' },
      ]
    }));
  });

  it('handles ADD_FEELING when empty', () => {
    let initialState = Map();
    let action = {type:'ADD_FEELING', feeling: { emotion: 'love', A: 'C', B: 'D' } };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({feelings:
      [
        { emotion: 'love', A: 'C', B: 'D' },
      ]
    }));
  });
  

  it('handles UPD_FEELING when empty', () => {
    let initialState = Map();
    let action = {
      type:'UPD_FEELING', 
      update: {
        feeling: {
          emotion: 'love',
          A: 'C',
          B: 'D'
        }
      }
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(Map());
  });
  
  it('handles UPD_FEELING', () => {
    let initialState = fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
      ]
    });
    let action = {
      type:'UPD_FEELING', 
      update: {
        feeling: {
          emotion: 'hatred',
          A: 'A',
          B: 'B'
        }
      }
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'hatred'},
        { A:'B', B:'C', emotion: 'hatred'},
      ]
    }));
  });
  
  it('handles UPD_FEELING (A & B not matching)', () => {
    let initialState = fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
      ]
    });
    let action = {
      type:'UPD_FEELING', 
      update: {
        feeling: {
          emotion: 'hatred',
          B: 'A',
          A: 'B'
        }
      }
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({feelings:
      [
        { A:'B', B:'A', emotion: 'hatred'},
      ]
    }));
  });



  it('handles UPD_FEELING when not existing', () => {
    let initialState = fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
      ]
    });
    let action = {
      type:'UPD_FEELING', 
      update: {
        feeling: {
          emotion: 'hatred',
          B: 'A',
          A: 'C'
        }
      }
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
      ]
    }));
  });
  

  it('handles DEL_FEELING', () => {
    let initialState = fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
        { emotion: 'love', A: 'C', B: 'D' },
      ]
    });
    let action = {type:'DEL_FEELING', id: 2 };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({feelings:
      [
        { A:'A', B:'B', emotion: 'love'},
        { A:'B', B:'C', emotion: 'hatred'},
      ]
    }));
  });









  it('handles ADD_TABLE', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
        't2': {name:'t2', size: 10, head: 'B'},
      }});
    let action = {type:'ADD_TABLE', name: 't3', size: 10, head: 'C' };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
        't2': {name:'t2', size: 10, head: 'B'},
        't3': {name:'t3', size: 10, head: 'C'},
      }
    }
    ));
  });

  it('handles ADD_TABLE with unexisting head', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
        't2': {name:'t2', size: 10, head: 'B'},
      }});
    let action = {type:'ADD_TABLE', name: 't3', size: 10, head: 'Z' };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
        't2': {name:'t2', size: 10, head: 'B'},
      }
    }
    ));
  });

  it('handles ADD_TABLE when empty', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      }
    });
    let action = {type:'ADD_TABLE', name: 't1', size: 10, head: 'A' };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
      }
    }));
  });
  



  it('handles UPD_TABLE', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {tablename:'t1', size: 10, head: 'A'},
      }
    });
    let action = {
      type:'UPD_TABLE',
      name: 't1',
      size: 12,
      head: 'C'
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 12, head: 'C'},
      }
    }));
  });


  it('handles UPD_TABLE when not there', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
      }
    });
    let action = {
      type:'UPD_TABLE',
      name: 't1',
      size: 12,
      head: 'C'
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
      }
    }));
  });
  
  it('handles DEL_TABLE', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
        't2': {name:'t2', size: 10, head: 'B'},
        't3': {name:'t3', size: 10, head: 'C'},
      }
    });
    let action = {type:'DEL_TABLE', name: 't2' };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
      tables: {
        't1': {name:'t1', size: 10, head: 'A'},
        't3': {name:'t3', size: 10, head: 'C'},
      }
    }));
  });













  it('handles ADD_PERSON', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
    });
    let action = {type:'ADD_PERSON', fullname: 'AA bb', plusone: true, id: 'X' };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
        'X': {id: 'X', fullname: 'AA bb', plusone: true },
      },
    }
    ));
  });

  it('handles UPD_PERSON', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
        'X': {id: 'X', fullname: 'AA bb', plusone: false },
      },
    });
    let action = {
      type:'UPD_PERSON',
      fullname: 'gggg',
      plusone: true,
      id: 'X'
    };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
        'X': {id: 'X', fullname: 'gggg', plusone: true },
      },
    }
    ));
  });

  it('handles UPD_PERSON when not there', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
    });
    let action = {
      type:'UPD_PERSON',
      update: {
        oldid: 'Y',
        person: {
          fullname: 'AA bb',
          plusone: true,
          id: 'X'
        }
      }
    };

    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
    }
    ));
  });
  
  it('handles ADD_PERSON when empty', () => {
    let initialState = fromJS({
    });
    let action = {type:'ADD_PERSON', person: { fullname: 'AA bb', plusone: true, id: 'X' } };
    let nextState = reducer(initialState, action);

    expect(nextState.size).to.equal(1);
  });
  
  it('handles DEL_PERSON', () => {
    let initialState = fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'C': {id: 'C', fullname: 'C c', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
    });
    let action = {type:'DEL_PERSON', id: 'C' };
    let nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      persons: {
        'A': {id: 'A', fullname: 'A a', plusone: true },
        'B': {id: 'B', fullname: 'cc bc', plusone: true },
        'D': {id: 'D', fullname: 'Cha c', plusone: true },
      },
    }));
  });





});
